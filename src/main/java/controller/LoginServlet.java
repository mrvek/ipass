package controller;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Medewerker;
import model.Patient;
import model.Provider;

public class LoginServlet extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		try{
			//try statement filtert parse-errors voor de volgende regel weg
			int username = Integer.parseInt(req.getParameter("IDnummer"));
			String pass1 = req.getParameter("Password");
			
			
			//Deze applicatie haalt bij login de data van de benodigde user(s) op en bij uitloggen worden deze vernietigd (nullified)
			//Dit ten opzichte van een database als backupsystem heeft als voordeel dat het minder ram verbruikt, sneller opstart en
			//sneller inlogt
			Patient userpatient = Provider.getService().loginPatient(username, pass1);
			Medewerker usermedewerker = Provider.getService().loginMedewerker(username, pass1);
			
			if (userpatient != null && usermedewerker == null) {
				Cookie cook = new Cookie("gebruikersnaam", req.getParameter("username"));
				resp.addCookie(cook);
				
				req.getSession().setAttribute("loggedUser", userpatient);
				String type = userpatient.getClass().getSimpleName();
				System.out.println(type);
				//Naast de user zelf, wordt de class van de user apart meegegeven, zodat webpagina's met een menubalk niet altijd de user-object hoeft op te vragen
				req.getSession().setAttribute("typeUser", type);
				
				System.out.println("Welcome back, " + userpatient.getVoornaam() + "!");
				resp.sendRedirect(req.getContextPath() + "/loggedIn/Patient/Pijnmetingen.jsp");
			}
			else if(usermedewerker != null && userpatient == null) {
				Cookie cook = new Cookie("gebruikersnaam", req.getParameter("username"));
				resp.addCookie(cook);
				System.out.println("YES!");
				
				req.getSession().setAttribute("loggedUser", usermedewerker);
				String type = usermedewerker.getClass().getSimpleName();
				System.out.println(type);
				req.getSession().setAttribute("typeUser", type);
				
				System.out.println("Welcome back, " + usermedewerker.getVoornaam() + "!");
				resp.sendRedirect(req.getContextPath() + "/loggedIn/Medewerker/Klantenlijst.jsp");
			}
			
			else {
				System.out.println("No match found");
				req.getSession().setAttribute("loggedUser", null);
				req.getSession().setAttribute("typeUser", null);
				resp.sendRedirect(req.getContextPath() + "/login.html");
			}
		}catch (NumberFormatException n) {
			n.printStackTrace();
			System.out.println("No match found");
			req.getSession().setAttribute("loggedUser", null);
			req.getSession().setAttribute("typeUser", null);
			resp.sendRedirect(req.getContextPath() + "/login.html");
		}
	}
}