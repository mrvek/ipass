package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Medewerker;
import model.Patient;
import model.Provider;

public class LogoutServlet extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException{
		HttpSession session = req.getSession();
		System.out.println(session);

		//hieronder wordt niet alleen de user uitgelogd, maar ook de userobject samen met eventuele koppelingen verwijderd (nullified) mits deze data niet ergens anders nodig zijn
		//nullified loose data (alle data waarvan de waarde null is en geen verwijzingen meer naar zijn, maar geen attribute is van een class) wordt automatisch door de jvm garbage collector (http://stackoverflow.com/questions/5757552/deleting-an-object-in-java)
		//jvm garbage collector wordt niet handmatig geactiveerd omdat het dan voortaan altijd handmatig geactiveerd moet worden (zie link voor duidelijkheid)
			if(session.getAttribute("typeUser").equals("Patient")) {
				Provider.getService().goodbyePatient((Patient) session.getAttribute("loggedUser"));
			}else if (session.getAttribute("typeUser").equals("Medewerker")) {
				Provider.getService().goodbyeMedewerker((Medewerker) session.getAttribute("loggedUser"));
			}
			
			
			
			session.removeAttribute("loggedUser");
			session.removeAttribute("typeUser");
			session.invalidate();
			resp.sendRedirect(req.getContextPath() + "/login.html");
		    
		
	}

}