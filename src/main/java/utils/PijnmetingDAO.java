package utils;

import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Patient;
import model.Pijnmeting;
import model.Provider;

public class PijnmetingDAO extends BaseDAO{
	
	public void checkpijnmetingenbijID(List<Integer> list) {	
		String querryin = null;
		if (!list.isEmpty()) {
			for (Integer meting : list) {
				if (querryin == null) {
					querryin = "" + meting;
				}else{
				querryin += "," + meting;
				}
			}
		}else{
			querryin = "" + 1;
		}
		
		String query = "SELECT * from pijnmeting WHERE idpijnmeting NOT IN (" + querryin + ")";
		
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);

			while(dbResultSet.next()) {
				int ID = dbResultSet.getInt("idpijnmeting");
				int PatientID = dbResultSet.getInt("idpatient");
				int hoeveelheid = dbResultSet.getInt("hoeveelheid");
				String locatie = dbResultSet.getString("locatie");
				String opmerking = dbResultSet.getString("opmerking");
				String datum = dbResultSet.getString("datum");
				String tijd = dbResultSet.getString("tijd");
				
				Provider.getService().createPijnmetingForUser(ID, PatientID, hoeveelheid, locatie, opmerking, datum, tijd);
			}
			con.close();
			
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

	public int createPijnmeting(int PatientID, int hoeveelheid, String locatie, String opmerking, String datum, String tijd) {
		
		try (Connection con = super.getConnection()) {
			//Er zijn database statements die gemaakt worden met con.prepareStatement(query)
			//Dit is vanwege de bescherming die deze statements kunnen bieden tegen sql-injections
			//overige tijd note: alle query's omzetten naar preparedStatements
			 if(opmerking == null || opmerking == "") {opmerking = "geen";}
			 String query = "INSERT INTO pijnmeting (idpatient, hoeveelheid, locatie, opmerking, datum, tijd) VALUES (?, ?, ?, ?, ?, ?)";
			 
			 java.sql.PreparedStatement pstmt = con.prepareStatement( query );
			 pstmt.setLong( 1, PatientID); 
			 pstmt.setLong(2, hoeveelheid);
			 pstmt.setString(3, locatie);
			 pstmt.setString(4, opmerking);
			 pstmt.setString(5, datum);
			 pstmt.setString(6, tijd);
			 pstmt.executeUpdate();
			
			 
			String query2 = "Select idpijnmeting FROM pijnmeting WHERE hoeveelheid = ? AND locatie = ? ANd opmerking = ? AND datum = ? AND tijd = ?";
			
			java.sql.PreparedStatement pstmt2 = con.prepareStatement( query2 );

			 pstmt2.setString(1, locatie);
			 pstmt2.setString(2, opmerking);
			 pstmt2.setString(3, datum);
			 pstmt2.setString(4, tijd);
			 ResultSet dbResultSet = pstmt2.executeQuery();
			
			dbResultSet.first();
			int ID = dbResultSet.getInt("idpijnmeting");
			con.close();
			return ID;
			
	}catch (SQLException sqle) {
		sqle.printStackTrace();
	}
		return 0;
}

	public void verwijder(Pijnmeting p) {
		String query = "DELETE FROM pijnmeting WHERE idpijnmeting = " + p.getID();
		
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
			
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		
	}


	public List<Pijnmeting> getPijnmetingforUser(Patient newuser) {
		List<Pijnmeting> list = new ArrayList<>();
		
		String querryin = null;
		if(newuser.getMijnpijnmetingen().isEmpty() == false){
			for (Pijnmeting user : newuser.getMijnpijnmetingen()) {
				if (querryin == null) {
					querryin = "" + user.getID();
				}else{
				querryin += "," + user.getID();
				}
			}
		}else{
			querryin = "" + 0;
		}
		
		String query = "SELECT * from pijnmeting WHERE idPatient = " + newuser.getID() + " AND idpijnmeting NOT IN (" + querryin + ")";
				
			try (Connection con = super.getConnection()) {
				Statement stmt = con.createStatement();
				ResultSet dbResultSet = stmt.executeQuery(query);

				while(dbResultSet.next()) {
					int ID = dbResultSet.getInt("idpijnmeting");
					int hoeveelheid = dbResultSet.getInt("hoeveelheid");
					String locatie = dbResultSet.getString("locatie");
					String opmerking = dbResultSet.getString("opmerking");
					String datum = dbResultSet.getString("datum");
					String tijd = dbResultSet.getString("tijd");
					
					Pijnmeting newmeting = new Pijnmeting (ID, hoeveelheid, locatie, opmerking, datum, tijd);
					list.add(newmeting);
				}
				con.close();
				return list;
			}catch (SQLException sqle) {
				sqle.printStackTrace();
			}
			return list;
	}
}